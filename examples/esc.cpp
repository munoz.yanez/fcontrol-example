
#include "fcontrol.h"
#include <math.h>

#include "IPlot.h"

double fJ(double wTarget, double wCurrent);


int main()
{

    double Ts=0.01;

    //SystemBlock Motor
    //numerator parameters
    std::vector<double> num={ 0.0002647, 0.005048};

    //denominator parameters
    std::vector<double> den={2.046e-09,-0.9923 ,1};

    //instantiate object
    SystemBlock motor(num,den);

//    SystemBlock filter(0.00995,0,- 0.99,1); //1 rad/s NOT WORKING!!
//    SystemBlock filter(0.009516,0,- 0.9048,1); //10 rad/s
    SystemBlock filter(0.009063,0,- 0.8187,1); //20 rad/s

    double fdjdu=0;


    IPlot pVt(Ts,"Position vs time ", "Time (s)", "Position signal (rad) ");




    double cs=0;
    double err=0;
    double y=0,y0=0;
    double du=0,u0=0,u=0;
    double djdu=0.1,dJ=0;

    double J=0,J0;
    double w=3;

    //Control loop
    for(int i=0;i<10000;i++)
    {

//        y0=y;
//        y = u > motor;


        y=motor.OutputUpdate(u);

        err = w-y;



        J0=J;
        J=fJ(w,y);
        dJ=(J-J0);

        if (abs(err)>0.001)
        {
            du = u-u0;

            if (abs(du)<0.000000001)
            {
                djdu=-0.1;
                fdjdu = djdu > filter;
            }
            else
            {


                djdu= dJ / du;
                fdjdu = djdu > filter;

            }
        }

        u0=u;
        u = u-0.04*fdjdu;


        pVt.pushBack(y);

    }

    pVt.Plot();


    return 0;
}


double fJ(double wTarget, double wCurrent)
{
    return (wTarget-wCurrent)*(wTarget-wCurrent);
}
