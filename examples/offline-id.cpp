#include <complex>

#include "fcontrol.h"
#include <math.h>
#include <deque>

#include "IPlot.h"

using namespace std;
const std::string NOMBRE_ARCHIVO = "IdSystemData.csv";
std::ifstream data(NOMBRE_ARCHIVO);
int main()
{
    string valores;
    char DELIMITADOR = ' ';
    getline(data, valores);
    std::vector<double> in,out,t;
    while (getline(data, valores)){
            stringstream stream(valores); // Convertir la cadena a un stream
            string input, output,trash,time;
            // Extraer todos los valores de esa fila
            getline(stream, time, DELIMITADOR);
            getline(stream, input, DELIMITADOR);
            getline(stream, input, DELIMITADOR);
            getline(stream, output, DELIMITADOR);
            getline(stream, trash, DELIMITADOR);
            getline(stream, trash, DELIMITADOR);
            in.push_back(stod(input));
            out.push_back(stod(output));
            t.push_back(stod(time));
    }

    double dts=0.02;
    SystemBlock sys(vector<double>{ 0 ,    0 ,  0.008337141952312  ,-0.015779442449882  , 0.007493362739673        ,0},vector<double>{-0.813867030581406 ,  4.20293861541632,  -8.717849112092322,   9.081650142885303,  -4.752787415097272 ,  1.000000000000000});


    int numOrder=sys.GetNumOrder(),denOrder=sys.GetDenOrder();
    OnlineSystemIdentification Gz(numOrder+2,denOrder+2,0.99);

    IPlot real(dts,"t(s)","Out","real");
    IPlot error(dts,"t(s)","Error","Identificado");

    double tmax=(in.size()-1)*dts;
    double iderror=0;

    for (double t=0,i=0; t<tmax; t+=dts,i++)
    {
        iderror=Gz.UpdateSystem(in[i],out[i]);
        real.pushBack(out[i]);
        error.pushBack(out[i]);
    }
    real.Plot();
    error.Plot();

    Gz.PrintZTransferFunction(dts);
    sys.PrintZTransferFunction(dts);
    return 0;
}

