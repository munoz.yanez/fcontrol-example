#include <complex>

#include "fcontrol.h"
#include <math.h>

#include <plotter.h>

using namespace std;

int main()
{

double dts=0.01;

//ans =

//  0.3923 z^4 - 1.558 z^3 + 2.321 z^2 - 1.536 z + 0.3813
//  -----------------------------------------------------
//     z^4 - 3.996 z^3 + 5.988 z^2 - 3.988 z + 0.9962
SystemBlock fPD (
            std::vector<double> {0.3813, - 1.536, 2.321, - 1.558, 0.3923 },
            std::vector<double> {0.9962,  - 3.988, 5.988, - 3.996, 1 });


//    FractionalController1DOF f1(1,dts);
    SystemBlock ipart1 (
                std::vector<double> {0,1*dts},
                std::vector<double> {-1,1});
    SystemBlock ipart2(
                std::vector<double> {0,1},
                std::vector<double> {-1,1});
FDeriv f1(0.99,1);

    for (int i=0; i<10; i++)
    {
        cout << "; State: " << (10 > ipart1 > ipart2 > f1  );

//        cout << "State: " << ipart2.GetState() << endl;

    }
//    for (int i=0; i<10; i++)
//    {
//        -5 > f1;
//        cout << "State: " << f1.GetState() << endl;

//    }
//    cout << "Final State: " << f1.GetState() << endl;
    return 0;
}
